/// <reference types="Cypress" />

describe('Testes Sprint 0', () => {

    beforeEach(() => {
        cy.visit('http://localhost:1234/')
      })

    it('Acessar site e verificar título', () =>{
        cy.title().should('be.equal', 'HOLPE')
        cy.get('.MuiTypography-root').should('contain', "Help with Hope")
    })
    it('Acessar página de Login', () =>{
        cy.get('.Component-root-1 > .MuiGrid-align-items-xs-center > .MuiButtonBase-root > .MuiButton-label').click()
    })

    it('Preencher formulários de login e enviar', () =>{
        cy.get('.Component-root-1 > .MuiGrid-align-items-xs-center > .MuiButtonBase-root > .MuiButton-label').click()
        cy.get(':nth-child(4) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input').type('teste@teste.com.br')
        cy.get(':nth-child(5) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input').type('123')
        cy.get('.MuiButton-label > .MuiGrid-container > .MuiGrid-grid-xs-12').click()
    })

    it('Acessar página de Cadastro', () =>{
        cy.get('.Component-root-1 > .MuiGrid-align-items-xs-center > .MuiButtonBase-root > .MuiButton-label').click()
        cy.get('a').click()
    })

    it('Preencher formulários de cadastro e enviar', () =>{
        cy.get('.Component-root-1 > .MuiGrid-align-items-xs-center > .MuiButtonBase-root > .MuiButton-label').click()
        cy.get('a').click()
        cy.get(':nth-child(3) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input').type('teste@teste.com.br')
        cy.get(':nth-child(3) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input').should('have.value', "teste@teste.com.br")
        
        cy.get(':nth-child(4) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input').type('123')
        cy.get(':nth-child(4) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input').should('have.value', "123")
        
        cy.get(':nth-child(5) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input').type('123')
        cy.get(':nth-child(5) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input').should('have.value', "123")
        
        cy.get(':nth-child(6) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input').type('1234')
        cy.get(':nth-child(6) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input').should('have.value', '1234')
        
        cy.get('.MuiButton-label > .MuiGrid-container > .MuiGrid-grid-xs-12').click()
    })

})